<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'HomeController')->name('home');
Route::get('/about.html', 'AboutController')->name('about');
Route::get('/tin-tuc/{slug}.html', 'BlogController')->name('blog.category');
Route::get('chi-tiet-tin-tuc/blog/{news}.html', 'Blog_allController@blog')->name('detail.Blog');
Route::get('/tin-tuc.html', 'Blog_allController')->name('blog');
Route::get('/search', 'Blog_allController@getSearch')->name('blog.search');
Route::get('/search', 'Blog_allController@getSearch')->name('blog.search');
Route::get('/lien-he.html', 'ContactController')->name('contact');
Route::post('/lien-he.html', 'ContactController@feedback')->name('contact.Feedback');
Route::post('/subscriber', 'SubscribersController')->name('subscriber');
Route::get('/loai-phong.html', 'RoomController')->name('room');
Route::get('chi-tiet-phong/{room}.html', 'DetailsRoomController')->name('detail.Room');
Route::post('/dat-phong', 'BookingRoomController')->name('booking');



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;
use Redirect,Response;

class SubscribersController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $feedback = Subscriber::create($request->all());
        $response = array(
            'status' => 'success',
            'msg' => 'Article has been posted.',
        );
        return Response::json($response);
    }
}

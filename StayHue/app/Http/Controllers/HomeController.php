<?php

namespace App\Http\Controllers;

use App\Banner;
use App\ImageRoom;
use App\Information;
use App\StyleRoom;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Category;
use TCG\Voyager\Models\Post;
use App\ServiceOther;

class HomeController extends Controller
{
    public function   __construct()
    {
        $styleRoom = StyleRoom::all()->first();
        view()->share('styleRoom',$styleRoom);
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Post $postModel, ServiceOther $serviceOtherModel, StyleRoom $styleRoomModel,ImageRoom $imageRoomModel,Information $informationModel, Category $categoryModel)
    {
        $information = $informationModel->get()->first();
        $posts = $postModel->where('status', 1)->orderBy('created_at', 'desc')->limit(4)->get();
        $styleRooms = $styleRoomModel->limit(3)->get();
        $imageRooms = $imageRoomModel->get();
        $categories = $categoryModel->get();
        $servicess =$serviceOtherModel->get();
        $viewData = compact('posts','styleRooms','imageRooms','information','categories','servicess');
        return view('pages.home',$viewData);
    }
}

<?php

namespace App\Http\Controllers;
use App\BookingRoom;
use App\Information;
use Illuminate\Http\Request\bookingRoomRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class BookingRoomController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        $bookingRoom = BookingRoom::create($request->all());
        $hotel = Information::get()->first();
        $data=  ['date_of_arrival'=>$request->date_of_arrival,
                'date_of_department'=>$request->date_of_department,
                'nameuser'=>$request->name,
                'quality'=>$request->quality,
                'style_room'=>$request->style_room,
                'phoneuser'=>$request->phone,
                'emailuser'=>$request->email,
                'name'=>$hotel->name,
                'address'=>$hotel->address,
                'phone'=>$hotel->phone,
                'emailhotel'=>$hotel->email,
                'image'=>$hotel->logo,
                ];
        Mail::send('shared.action.sendmail',$data,function($message){
            $mails = BookingRoom::all()->last();
            $hotel = Information::get()->first();
            $message->from('hdlevent26@gmail.com',$hotel->name);
            $message->to($mails->email)->subject('Xác nhận đặt phòng');
        });

        return redirect()->back()->with('message','Cảm ơn bạn đã đặt phòng, xin vui lòng kiểm tra lại email. Chúng tôi sẽ liên hệ bạn trong thời gian sớm nhất');
    }
}
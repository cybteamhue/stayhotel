<?php

namespace App\Http\Controllers;

use App\Information;
use App\StyleRoom;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Category;
use App\ServiceOther;

class RoomController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, StyleRoom $styleRoomModel, ServiceOther $serviceOtherModel,Information $informationModel, Category $categoryModel)
    {
        $information = $informationModel->get()->first();
        $styleRooms = $styleRoomModel->get();
        $categories = $categoryModel->get();
        $servicess =$serviceOtherModel->get();
        $viewData = compact('styleRooms','information','categories','servicess');
        return view('pages.booking_room',$viewData);
    }
}

@extends('layouts.layoutsroom')

@push('head')
<meta name="keywords" content="{{ $post->title }}">
<meta property="og:title" content="{{ $post->title }}">
<meta property="og:description" content="{!! $post->excerpt !!}">
<meta property="og:url" content="{{ asset('') }}/blog/{{ $post->slug }}">
<link rel="canonical" href="{{ asset('') }}/blog/{{ $post->slug }}">
<meta property="og:image" content="{{ Voyager::image( method_exists($post, 'thumbnail') ? $post->thumbnail('cropped') : $post->image ) }}">
<meta property="og:image:alt" content="{{ $post->name }}">
<meta property="og:image:width" content="819">
<meta property="og:image:height" content="1024">
@endpush
@section('content')
    <main>              
        @include('sections.booking_room.results')
        @include('sections.booking_room.filters_listing')     
        <div class="container margin_60_35 clear">               
            <div class="row">               
                @include('sections.detail_blog.description')
                @include('sections.detail_blog.description_booking')
            </div>
        </div>    
    </main>
@endsection
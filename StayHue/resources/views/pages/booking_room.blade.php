@extends('layouts.layoutsroom')
@section('title')
{{$information->name}} - Đặt phòng
@endsection
@section('content')
    <main>              
        @include('sections.booking_room.results')
        @include('sections.booking_room.filters_listing')     
        <div class="container margin_60_35">               
            <div class="row">               
                @include('sections.booking_room.sidebarroom')
                @include('sections.booking_room.room')
            </div>
        </div>    
    </main>
@endsection
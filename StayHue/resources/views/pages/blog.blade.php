@extends('layouts.layoutsroom')
@section('title')
{{$information->name}} - Tin tức
@endsection
@section('content')
    <main>              
        @include('sections.booking_room.results')
        @include('sections.booking_room.filters_listing')     
        <div class="container margin_60_35">               
            <div class="row">               
                @include('sections.blog.blog_all.blog_category')
                <aside class="col-lg-3">
                    @include('sections.blog.blog_all.search')
                    @include('sections.blog.category_blog')

                </aside>
            </div>
        </div>    
    </main>
@endsection
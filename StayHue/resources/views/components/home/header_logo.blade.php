{{-- <div class="navigation__{{ $position }}">
    <ul class="menu menu--{{ $position }}">
        {{ menu($position . ' header menu', 'components.custom-menu') }}
    </ul>
</div> --}}
<div id="logo">
    <a href="{{ route('home') }}" title="Sparker - Directory and listings template">
        <img src="{{ Voyager::image( method_exists($information, 'thumbnail') ? $information->thumbnail('cropped') : $information->logo ) }}" width="165" height="35" alt="" class="logo_normal">
        <img src="{{ Voyager::image( method_exists($information, 'thumbnail') ? $information->thumbnail('cropped') : $information->logo ) }}" width="165" height="35" alt="" class="logo_sticky">
    </a>
</div>



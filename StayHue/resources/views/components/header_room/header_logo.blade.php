<div class="col-lg-3 col-12">
        <div id="logo">
            <a href="{{ route('home') }}">
                <img src="{{ Voyager::image( method_exists($information, 'thumbnail') ? $information->thumbnail('cropped') : $information->logo ) }}" width="165" height="35" alt="" class="logo_normal">
        </div>
</div>
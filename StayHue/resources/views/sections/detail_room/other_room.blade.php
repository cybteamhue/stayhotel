<h3>CÓ THỂ BẠN QUAN TÂM</h3>
@foreach($styleRooms as $styleRooms)
        @if($styleRoom->id != $styleRooms->id)
            <div class="room_type first">
                <div class="row">
                    <div class="col-md-4">
                        <a href="{{ route('detail.Room',$styleRooms->slug) }}">   <img src="{{ Voyager::image( method_exists($styleRooms, 'thumbnail') ? $styleRooms->thumbnail('cropped') : $styleRooms->image ) }}"  class="img-fluid" alt=""></a>
                    </div>
                    <div class="col-md-8">
                        <h3><a href="{{ route('detail.Room',$styleRooms->slug) }}">{{ $styleRooms->name }}</a></h3>
                        <h5>{{number_format($styleRooms->price, 0, ',', ',')}} <small>VNĐ/NGÀY</small></h5>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /rom_type -->
        @endif
    
@endforeach 
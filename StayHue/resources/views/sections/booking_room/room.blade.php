<div class="col-lg-9">
        <div class="row">
            @foreach($styleRooms as $styleRoom)
            <div class="col-md-6">
                <div class="strip grid">
                    <figure>
                        <a  class="wish_bt"></a>
                        <a href="{{ route('detail.Room',$styleRoom->slug) }}"><img src="{{ Voyager::image( method_exists($styleRoom, 'thumbnail') ? $styleRoom->thumbnail('cropped') : $styleRoom->image ) }}" class="img-fluid" alt="{{$styleRoom->name}}">
                            <div class="read_more"><span>Xem chi tiết</span></div>
                        </a>
                       
                    </figure>
                    <div class="wrapper">
                        <h3><a href="{{ route('detail.Room',$styleRoom->slug) }}">{{ $styleRoom->name }}</a></h3>
                        <p>{!! $styleRoom->contents !!}</p>
                        <h4>{{number_format($styleRoom->price, 0, ',', ',')}} <small>VNĐ/NGÀY</small></h4>

                    </div>
                    <ul>
                        <li><span class="loc_open"> <a href="{{ route('detail.Room',$styleRoom->slug) }}">Xem chi tiết</a></span></li>
                        <li>
                            <div class="score"><span>STAY HUE<em>HOTEL</em></span><strong></strong></div>
                        </li>
                    </ul>
                </div>
            </div>
            @endforeach
            <!-- /strip grid -->            
        </div>
        <!-- /row -->        
    </div>
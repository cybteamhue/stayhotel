<div class="bg_color_1">
    <div class="container margin_80_55">
        <div class="main_title_2">
            <span><em></em></span>
            <h2></h2>
          
        </div>
        <div class="row justify-content-between">
            <div class="col-lg-6 wow" data-wow-offset="150">
                <figure class="block-reveal">
                    <div class="block-horizzontal"></div>
                    <img src="{{ Voyager::image( method_exists($information, 'thumbnail') ? $information->thumbnail('cropped') : $information->image) }}" class="img-fluid" alt="">
                </figure>
            </div>
            <div class="col-lg-6">
                <p> {!! $information->contents !!}</p>
               
            </div>
        </div>
        <!--/row-->
    </div>
    <!--/container-->
</div>
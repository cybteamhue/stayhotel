<!DOCTYPE html>
<html lang="en">
    @include('shared.head')    
    <body>
        @include('shared.containers.default')
        @include('shared.foot')
    </body>
</html>

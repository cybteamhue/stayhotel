
<div class="fb-page" data-href="https://www.{{ $information->facebook }}" data-tabs="timeline" data-width="" data-height="230" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
    <blockquote cite="{{ $information->facebook }}" class="fb-xfbml-parse-ignore">
        <a href="https://www.{{ $information->facebook }}">{{ $information->name }}</a>
    </blockquote>
</div>

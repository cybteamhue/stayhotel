<header class="header_in sticky_horizontal">
		<div class="container ">
			<div class="row">
                    @include('components.header_room.header_logo')
					@include('components.header_room.top_menu')	
					              
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
		<!-- search_mobile -->
		<div class="layer"></div>
		@include('components.header_room.search_mobile')	
		<!-- /search_mobile -->
</header>
-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 30, 2019 lúc 09:16 AM
-- Phiên bản máy phục vụ: 10.4.6-MariaDB
-- Phiên bản PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `stayhotel`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `advertisements`
--

CREATE TABLE `advertisements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `styleAdv_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `advertisements`
--

INSERT INTO `advertisements` (`id`, `name`, `email`, `phone`, `image`, `website`, `styleAdv_id`, `created_at`, `updated_at`) VALUES
(2, 'Anh 5', 'long99@gmail.com', '0966995732', 'support.bannerWEB-02.jpg', 'https://www.facebook.com/Motel-Ho%C3%A0ng-Ph%C3%BAc-100187004708537/', 1, '2019-09-25 20:00:40', '2019-09-26 19:09:25'),
(3, 'Anh 7', 'long99@gmail.com', '3423424242', 'lễ tân 1.jpg', 'facebook.com/Motel-Hoàng-Phúc-100187004708537/', 2, '2019-09-25 20:01:41', '2019-09-30 20:04:19');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_loaiphong` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `images`
--

INSERT INTO `images` (`id`, `name`, `image`, `id_loaiphong`, `created_at`, `updated_at`) VALUES
(3, 'Deluxe river view / Family river view 1', 'IMG_8318.jpg', 4, '2019-09-17 01:32:23', '2019-10-09 06:53:08'),
(4, 'Deluxe river view / Family river view 2', 'IMG_8298.jpg', 4, '2019-09-17 01:45:10', '2019-10-09 06:53:43'),
(5, 'Deluxe river view / Family river view 3', 'IMG_8321.jpg', 4, '2019-09-17 01:45:44', '2019-10-09 06:54:14'),
(6, 'Superior river view 1', 'IMG_8304 (1).jpg', 5, '2019-09-17 01:46:43', '2019-10-09 06:54:34'),
(7, 'Superior river view 2', 'IMG_8305.jpg', 5, '2019-09-17 01:48:19', '2019-10-09 06:54:47'),
(8, 'Superior river view 3', 'IMG_8319.jpg', 5, '2019-09-17 01:48:52', '2019-10-09 06:55:08'),
(9, 'Superior garden/ city view 1', 'IMG_8304.jpg', 7, '2019-09-17 02:27:13', '2019-10-09 06:55:30'),
(10, 'Superior garden/ city view 2', 'IMG_8300.jpg', 7, '2019-10-03 02:52:55', '2019-10-09 06:55:44'),
(11, 'Superior garden/ city view 3', 'IMG_8310.jpg', 7, '2019-10-03 02:53:23', '2019-10-09 06:56:01');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `information`
--

CREATE TABLE `information` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `information`
--

INSERT INTO `information` (`id`, `name`, `address`, `phone`, `website`, `email`, `content`, `note`, `created_at`, `updated_at`) VALUES
(1, 'STAY HUE HOTEL', '7 Nguyễn Công Trứ, Tp. Huế', '02343823999', 'facebook.com/stayhuehotel/?ref=br_rs', 'stayhuehotel123@gmail.com', '<p style=\"text-align: justify;\"><span style=\"font-family: \'arial black\', \'avant garde\'; font-size: 14pt;\"><span style=\"font-family: \'times new roman\', times;\">Tọa lạc tại số 7 Nguyễn C&ocirc;ng Trứ th&agrave;nh phố Huế. Kh&aacute;ch sạn Stay cung cấp nơi lưu tr&uacute; đạt ti&ecirc;u chuẩn ngay trung t&acirc;m TP Huế. Chỉ mất 1p đi bộ l&agrave; đến ngay d&ograve;ng s&ocirc;ng Hương thơ mộng, 3p để đến cầu Tr&agrave;ng Tiền v&agrave; mất 20p đi đến kinh th&agrave;nh Huế. Kh&aacute;ch sạn Stay gần với c&aacute;c khu vực mua sắm cũng như c&aacute;c địa điểm ẩm thực v&agrave; khu vực mua sắm cũng như c&aacute;c địa điểm ẩm thực v&agrave; khu giải tr&iacute; của th&agrave;nh phố. Với đội ngũ nh&acirc;n vi&ecirc;n chuy&ecirc;n nghiệp, trẻ trung, nhiệt t&igrave;nh chắc chắn sẽ mang lại cho qu&yacute; kh&aacute;ch sự h&agrave;i l&ograve;ng, thoải m&aacute;i v&agrave; ấm c&uacute;ng như ở nh&agrave;. Điều đặc biệt l&agrave; qu&yacute; kh&aacute;ch c&oacute; thể nh&igrave;n được 2 d&ograve;ng s&ocirc;ng nổi tiếng của Huế l&agrave; s&ocirc;ng Hương v&agrave; Như &Yacute; từ ph&ograve;ng của kh&aacute;ch sạn. Với 35 ph&ograve;ng nghỉ đầy đủ tiện nghi, hệ thống wifi phủ khắp kh&aacute;ch sạn c&ugrave;ng với c&aacute;c th&ocirc;ng tin v&agrave; dịch vụ kh&aacute;c theo y&ecirc;u cầu của qu&yacute; kh&aacute;ch.</span></span></p>', '<p><label for=\"\">Kh&aacute;ch sạn Stay Hue Hotel với<strong>&nbsp;</strong>kh&ocirc;ng gian thoải m&aacute;i, nội thất ấm c&uacute;ng, đầy đủ c&aacute;c trang thiết bị tiện nghi. Wi-Fi miễn ph&iacute; trong tất cả c&aacute;c ph&ograve;ng v&agrave; khu vực c&ocirc;ng cộng, quầy lễ t&acirc;n v&agrave; dịch vụ ph&ograve;ng phục vụ 24/24 giờ, b&atilde;i đỗ xe miễn ph&iacute;.</label></p>\r\n<p><strong>C&aacute;c loại ph&ograve;ng của Kh&aacute;ch sạn Stay Hue Hotel&nbsp;gồm:</strong></p>\r\n<ul>\r\n<li>Superior garden/ city view: 10 ph&ograve;ng</li>\r\n<li>Superior river view: 10 ph&ograve;ng</li>\r\n<li>Deluxe river view / Family river view: 15 ph&ograve;ng</li>\r\n</ul>\r\n<p><strong>Giờ check-in:&nbsp;</strong>2PM<br /><strong>Giờ check-out</strong>: 12PM</p>\r\n<p>Nếu qu&yacute; kh&aacute;ch c&oacute; bất kỳ c&acirc;u hỏi g&igrave; về việc đặt ph&ograve;ng. Xin vui l&ograve;ng li&ecirc;n hệ với ch&uacute;ng t&ocirc;i qua số điện thoại: <strong>02343823999&nbsp;| 0905564275</strong></p>', NULL, '2019-10-09 18:23:56');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_06_20_101835_create_information_table', 1),
(4, '2019_06_20_135347_create_rooms_table', 1),
(5, '2019_06_20_135413_create_style_rooms_table', 1),
(6, '2019_06_20_135826_create_reservations_table', 1),
(7, '2019_06_20_135920_create_images_table', 1),
(8, '2019_06_20_140116_create_news__evens_table', 1),
(9, '2019_06_20_140204_create_services_table', 1),
(10, '2019_06_20_140301_create_advertisements_table', 1),
(11, '2019_06_21_015907_create_style_images_table', 1),
(12, '2019_06_24_082535_create_tours_table', 1),
(13, '2019_06_26_004930_connect', 1),
(14, '2019_07_03_074702_noiquy_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news__evens`
--

CREATE TABLE `news__evens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `news__evens`
--

INSERT INTO `news__evens` (`id`, `title`, `content`, `image`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Vu Lan Báo Hiếu - Ngày đại lễ của những người con', '<p style=\"font-weight: 400; text-align: center;\"><span style=\"font-size: 18pt;\"><strong>Vu Lan B&aacute;o Hiếu - Ng&agrave;y đại lễ của những người con</strong></span><br />&nbsp;</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">Đ&atilde; l&agrave; người Việt Nam th&igrave; chắc chắn ai cũng biết đến ng&agrave;y Lễ Vu Lan, v&agrave; cũng từng &iacute;t nhất một lần đến ch&ugrave;a tham dự ng&agrave;y đại lễ n&agrave;y. Hằng năm, tất cả c&aacute;c ch&ugrave;a d&ugrave; l&agrave; lớn hay nhỏ đều sẽ tổ chức Lễ Vu Lan, bởi đối với c&aacute;c tăng ni th&igrave; đ&acirc;y ch&iacute;nh l&agrave; một lễ lớn.</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">Đại Lễ Vu Lan sẽ được diễn ra v&agrave;o ng&agrave;y Rằm th&aacute;ng Bảy &Acirc;m Lịch hằng năm v&agrave; ng&agrave;y lễ năm nay sẽ rơi v&agrave;o ng&agrave;y 15/8/2019 dương lịch (15/7/2019 &acirc;m lịch).</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">Để tạo cơ hội cho c&aacute;c Phật tử về d&acirc;ng hương, trước l&agrave; cầu nguyện cho cha mẹ hiện tiền lu&ocirc;n lu&ocirc;n được b&igrave;nh an, khỏe mạnh, kế nữa l&agrave; c&aacute;c vị cha mẹ bảy đời qu&aacute; khứ được tho&aacute;t khỏi khổ ải nơi địa ngục, sớm về c&otilde;i Trời an lạc.</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">Kh&ocirc;ng chỉ ở Việt Nam của ch&uacute;ng ta, m&agrave; tất cả c&aacute;c ch&ugrave;a ở khắp nơi tr&ecirc;n thế giới đều tổ chức ng&agrave;y đại lễ n&agrave;y.</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">Cho n&ecirc;n khi nhắc tới ng&agrave;y Lễ Vu Lan th&igrave; mọi người sẽ ngay lập tức nghĩ ngay đ&oacute; ch&iacute;nh l&agrave; ng&agrave;y Lễ B&aacute;o Hiếu Cha Mẹ.</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">Một m&ugrave;a B&aacute;o Hiếu nữa lại sắp đến, đ&acirc;y ch&iacute;nh l&agrave; dịp để ch&uacute;ng ta sống chậm lại, tạm g&aacute;c lại những x&ocirc; bồ, hối hả trong cuộc sống m&agrave; d&agrave;nh thời gian để tự ngẫm v&agrave; y&ecirc;u thương nhiều hơn.</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">L&agrave; dịp để mỗi người con hướng l&ograve;ng th&agrave;nh k&iacute;nh về đấng sinh th&agrave;nh, những người đ&atilde; ban cho ch&uacute;ng ta cuộc sống tr&ecirc;n c&otilde;i đời n&agrave;y.</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">C&oacute; bao giờ bạn một lần tự hỏi ch&iacute;nh m&igrave;nh, đ&atilde; bao l&acirc;u rồi m&igrave;nh kh&ocirc;ng d&agrave;nh thời gian b&ecirc;n gia đ&igrave;nh, n&oacute;i những lời y&ecirc;u thương với cha mẹ?</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">Vậy th&igrave; đ&acirc;y ch&iacute;nh l&agrave; một cơ hội tuyệt vời, h&atilde;y ngay lập tức h&agrave;nh động, b&agrave;y tỏ l&ograve;ng tri &acirc;n, thể hiện sự k&iacute;nh y&ecirc;u tới cha mẹ m&igrave;nh.</p>\r\n<h3><strong>&Yacute; nghĩa b&ocirc;ng hồng c&agrave;i tr&ecirc;n ngực &aacute;o</strong></h3>\r\n<p style=\"font-weight: 400; text-align: justify;\">Trước đ&acirc;y, ng&agrave;y Lễ Vu Lan vốn được tổ chức v&agrave;o ng&agrave;y 14 v&agrave; 15 của th&aacute;ng 7 &acirc;m lịch. Nhưng mấy năm trở lại đ&acirc;y đ&atilde; trở th&agrave;nh một đại lễ, n&ecirc;n rất nhiều nơi đ&atilde; tổ chức Lễ Vu Lan k&eacute;o d&agrave;i suốt 30 ng&agrave;y trong th&aacute;ng 7 &acirc;m lịch.</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">Nghi thức &ldquo;B&ocirc;ng Hồng C&agrave;i &Aacute;o&rdquo; l&agrave; để tưởng nhớ những người mẹ đ&atilde; tạ thế, đồng thời để t&ocirc;n vinh những người mẹ c&ograve;n lại tr&ecirc;n thế gian n&agrave;y với c&aacute;c con c&aacute;c ch&aacute;u.</p>\r\n<p style=\"font-weight: 400; text-align: center;\"><img class=\"\" src=\"../../../source/vu%20lan%20b%C3%A1o%20hi%E1%BA%BFu.jpg\" alt=\"\" width=\"800\" height=\"432\" /></p>\r\n<p style=\"font-weight: 400; text-align: justify;\">Trong nghi thức, c&aacute;c Phật Tử với hai giỏ hoa hồng b&ecirc;n người, một giỏ hoa hồng đỏ v&agrave; một giỏ hoa hồng trắng, sẽ c&agrave;i hoa l&ecirc;n &aacute;o của người đến ch&ugrave;a tham dự lễ.</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">Như ch&uacute;ng ta đ&atilde; biết, hoa hồng ch&iacute;nh l&agrave; biểu tượng của sự cao qu&yacute; v&agrave; t&igrave;nh y&ecirc;u bất diệt.</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">Với &yacute; nghĩa thi&ecirc;ng li&ecirc;ng đ&oacute;, n&ecirc;n cứ đến ng&agrave;y lễ n&agrave;y, mọi người khi tham dự lễ sẽ được c&agrave;i một b&ocirc;ng hoa m&agrave;u hồng l&ecirc;n ngực &aacute;o, đ&oacute; l&agrave; &yacute; nghĩ của việc bạn c&ograve;n đầy đủ cả mẹ lẫn cha.</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">Theo như lời H&ograve;a thượng Th&iacute;ch Bảo Nghi&ecirc;m th&igrave;: L&uacute;c đầu, nghi thức n&agrave;y chỉ sử dụng hoa hồng m&agrave;u đỏ, nhưng về sau, một số nơi bắt đầu ph&acirc;n chia ra th&agrave;nh nhiều m&agrave;u sắc hoa hồng kh&aacute;c nhau.</p>\r\n<p style=\"font-weight: 400; text-align: center;\"><img class=\"\" src=\"../../../source/vu%20lan.jpg\" alt=\"\" width=\"800\" height=\"533\" /></p>\r\n<p style=\"font-weight: 400; text-align: justify;\">Như người n&agrave;o đ&atilde; mất hết cha c&ugrave;ng mẹ th&igrave; c&agrave;i hoa hồng m&agrave;u trắng, người n&agrave;o c&ograve;n đầy đủ cha mẹ th&igrave; c&agrave;i hoa hồng m&agrave;u đỏ, người n&agrave;o chỉ c&ograve;n mỗi cha hoặc l&agrave; mẹ th&igrave; sẽ c&agrave;i hoa hồng m&agrave;u nhạt hơn.</p>\r\n<p style=\"font-weight: 400; text-align: justify;\"><strong>Lễ vu lan n&ecirc;n l&agrave;m g&igrave;</strong></p>\r\n<p style=\"font-weight: 400; text-align: justify;\">Như ch&iacute;nh những &yacute; nghĩa m&agrave; ch&uacute;ng ta đ&atilde; l&yacute; giải ở tr&ecirc;n, ng&agrave;y Đại Lễ Vu Lan sẽ c&oacute; một số hoạt động ch&iacute;nh như sau:</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">1.Thể hiện l&ograve;ng nh&acirc;n từ đối với tất cả ch&uacute;ng sinh th&ocirc;ng qua nghi thức ph&oacute;ng sinh</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">2.Ăn chay, niệm phật, cầu an cho tổ ti&ecirc;n, &ocirc;ng b&agrave;, cha mẹ</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">3.Đến ch&ugrave;a thắp hương cầu nguyện, nghe c&aacute;c vị trụ tr&igrave; thuyết giảng gi&aacute;o l&yacute;</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">4.Chuẩn bị m&acirc;m cơm tươm tất tại nh&agrave; để d&acirc;ng l&ecirc;n Thần Phật, gia ti&ecirc;n để tỏ l&ograve;ng th&agrave;nh k&iacute;nh v&agrave; b&aacute;o hiếu</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">5.Tham dự Lễ Vu Lan v&agrave; c&agrave;i hoa l&ecirc;n ngực &aacute;o để tưởng nhớ c&ocirc;ng ơn lớn lao của đấng sinh th&agrave;nh.</p>\r\n<p style=\"font-weight: 400; text-align: justify;\">Bạn đ&atilde; hiểu được &yacute; nghĩa của ng&agrave;y lễ Vu Lan b&aacute;o hiếu rồi phải kh&ocirc;ng? H&atilde;y thể hiện t&igrave;nh cảm của m&igrave;nh d&agrave;nh cho ba mẹ &ndash;&nbsp; những người th&acirc;n y&ecirc;u nhất của m&igrave;nh trong những ng&agrave;y n&agrave;y nh&eacute;.</p>', 'vu lan báo hiếu.jpg', 'Vu Lan Báo Hiếu - Ngày đại lễ của những người con', '2019-09-17 19:57:33', '2019-09-17 19:57:33');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `noiquy`
--

CREATE TABLE `noiquy` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `reservations`
--

CREATE TABLE `reservations` (
  `id` int(10) UNSIGNED NOT NULL,
  `ngayden` date NOT NULL,
  `ngaydi` date NOT NULL,
  `soluong` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trangthai` tinyint(1) NOT NULL DEFAULT 0,
  `loaiphong` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hoten` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dienthoai` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `reservations`
--

INSERT INTO `reservations` (`id`, `ngayden`, `ngaydi`, `soluong`, `trangthai`, `loaiphong`, `hoten`, `dienthoai`, `email`, `created_at`, `updated_at`) VALUES
(1, '2019-09-20', '2019-09-22', '3', 0, 'Phòng đôi', 'long', '0776914759', 'manhlong99@gmail.com', '2019-09-17 02:33:00', '2019-09-17 02:33:00'),
(2, '2019-09-21', '2019-09-22', '3', 0, 'Phòng đôi', 'Long', '0776914759', 'manhlong99@gmail.com', '2019-09-17 02:35:18', '2019-09-17 02:35:18');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `rooms`
--

CREATE TABLE `rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `Sophong` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Mota` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Trangthai` tinyint(1) NOT NULL DEFAULT 0,
  `id_loaiPhong` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nameService` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `services`
--

INSERT INTO `services` (`id`, `nameService`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Dịch vụ nhà hàng và Cafe', '4834_cafe_quan_1_2.jpg', NULL, '2019-09-17 00:45:26'),
(2, 'Dịch Vụ Giặt Là (30.000đ/1kg)', 'tải xuống (2).jpg', NULL, '2019-09-17 00:45:41'),
(3, 'Dịch vụ thuê phương tiện đi lại', 'tải xuống (1).jpg', NULL, '2019-09-17 00:44:51'),
(4, 'Dịch vụ lữ hành và đặt vé máy bay', 'tải xuống.jpg', NULL, '2019-09-17 00:41:23'),
(5, 'Dịch vụ trông trẻ', 'images.jpg', '2019-09-17 00:47:18', '2019-09-17 00:47:18');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `style_advs`
--

CREATE TABLE `style_advs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `style_advs`
--

INSERT INTO `style_advs` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Anh 7', '2019-09-25 20:00:27', '2019-09-25 20:00:27'),
(2, 'Anh 6', '2019-09-25 20:01:28', '2019-09-25 20:01:28');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `style_images`
--

CREATE TABLE `style_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `nameStyle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `style_images`
--

INSERT INTO `style_images` (`id`, `nameStyle`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Ảnh quảng cáo', 'Hương Giang nằm bên bờ sông Hương thơ mộng và đi vào lòng người. Chúng tôi là khách sạn có thương hiệu và uy tín', 'https://bitly.vn/5j9k', NULL, NULL),
(2, 'Ảnh tin tức sự kiện', 'Hương Giang nằm bên bờ sông Hương thơ mộng và đi vào lòng người. Chúng tôi là khách sạn có thương hiệu và uy tín', 'https://bitly.vn/5j9k', NULL, NULL),
(3, 'Ảnh Dịch Vụ', 'Hương Giang nằm bên bờ sông Hương thơ mộng và đi vào lòng người. Chúng tôi là khách sạn có thương hiệu và uy tín', 'https://bitly.vn/5j9k', NULL, NULL),
(4, 'Ảnh Tour', 'Hương Giang nằm bên bờ sông Hương thơ mộng và đi vào lòng người. Chúng tôi là khách sạn có thương hiệu và uy tín', 'https://bitly.vn/5j9k', NULL, NULL),
(5, 'Ảnh Các Loại Phòng', 'Hương Giang nằm bên bờ sông Hương thơ mộng và đi vào lòng người. Chúng tôi là khách sạn có thương hiệu và uy tín', 'https://bitly.vn/5j9k', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `style_rooms`
--

CREATE TABLE `style_rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `TenLoaiPhong` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Gia` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Mota` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `style_rooms`
--

INSERT INTO `style_rooms` (`id`, `TenLoaiPhong`, `Gia`, `Mota`, `note`, `image`, `slug`, `created_at`, `updated_at`) VALUES
(4, 'Deluxe river view / Family river view', '500,000', '<p>Với 15 ph&ograve;ng nghỉ diện t&iacute;ch từ 25 m2</p>\r\n<p>Đầy đủ trang thiết bị đồ d&ugrave;ng , sinh hoạt: tủ lạnh, m&aacute;y lạnh, tivi, tủ đựng quần &aacute;o,....</p>\r\n<p>C&oacute; cửa sổ tho&aacute;ng m&aacute;t</p>', NULL, 'IMG_8298.jpg', 'deluxe-river-view-family-river-view', NULL, '2019-10-09 06:33:08'),
(5, 'Superior river view', '400,000', '<p>Với 10 ph&ograve;ng nghỉ diện t&iacute;ch từ 20 m2</p>\r\n<p>Đầy đủ trang thiết bị đồ d&ugrave;ng , sinh hoạt: tủ lạnh, m&aacute;y lạnh, tivi, tủ đựng quần &aacute;o,....</p>\r\n<p>C&oacute; cửa sổ tho&aacute;ng m&aacute;t</p>', NULL, 'IMG_8305.jpg', 'superior-river-view', NULL, '2019-10-09 06:29:54'),
(7, 'Superior garden/ city view', '350,000', '<p>Với 10 ph&ograve;ng nghỉ diện t&iacute;ch từ 20 m2</p>\r\n<p>Đầy đủ trang thiết bị đồ d&ugrave;ng , sinh hoạt: tủ lạnh, m&aacute;y lạnh, tivi, tủ đựng quần &aacute;o,....</p>\r\n<p>C&oacute; cửa sổ tho&aacute;ng m&aacute;t</p>', NULL, 'IMG_8449.jpg', 'superior-garden-city-view', '2019-09-29 21:44:14', '2019-10-09 06:29:40');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tours`
--

CREATE TABLE `tours` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nameTour` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tours`
--

INSERT INTO `tours` (`id`, `nameTour`, `image`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Huế City Tour - 1 Ngày ở Huế', 'eiv1_huetour.jpg', '<p>Tham quan Đại Nội, Ch&ugrave;a Thi&ecirc;n Mụ, Lăng Minh Mạng, Lăng Khải Định, Thuyền rồng tr&ecirc;n s&ocirc;ng Hương</p>', NULL, '2019-09-17 00:29:01'),
(2, 'Tour Khám phá ẩm thực xứ Huế', 'tkNY_PicMonkey Collage_resize.jpg', '<p>Thời gian: 18h00 - 22h00, Di chuyển bằng xe</p>', NULL, '2019-09-17 00:28:31'),
(3, 'Tour Hoàng hôn phá Tam Giang - Khám phá đầm phá lớn nhất', 'AQpY_phatamgiang.jpg', '<p>Thời gian: 1 đến 2 ng&agrave;y.</p>', NULL, '2019-09-17 00:28:50'),
(4, 'Tour Khám phá Bạch Mã Huế', 'Hvkl_1535607616264_9627017.jpg', '<p>Thời gian: 1 Ng&agrave;y</p>', NULL, '2019-09-17 00:27:47');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DOB` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT 1,
  `remember` tinyint(1) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `DOB`, `phone`, `email`, `email_verified_at`, `password`, `role`, `remember`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'long', '2019-09-21', '0966995732', 'long99@gmail.com', NULL, '$2y$10$A0g93WE9xXsN0O01Y4UxRuR9QbYsYuQ28fTEq8hTI7MTYVFEBe0l6', 1, 0, NULL, '2019-09-16 19:42:29', '2019-09-16 19:42:29');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `advertisements`
--
ALTER TABLE `advertisements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `styleAdv_id` (`styleAdv_id`);

--
-- Chỉ mục cho bảng `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `images_id_loaiphong_foreign` (`id_loaiphong`);

--
-- Chỉ mục cho bảng `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news__evens`
--
ALTER TABLE `news__evens`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `noiquy`
--
ALTER TABLE `noiquy`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rooms_id_loaiphong_foreign` (`id_loaiPhong`);

--
-- Chỉ mục cho bảng `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `style_advs`
--
ALTER TABLE `style_advs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `style_images`
--
ALTER TABLE `style_images`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `style_rooms`
--
ALTER TABLE `style_rooms`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tours`
--
ALTER TABLE `tours`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `advertisements`
--
ALTER TABLE `advertisements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT cho bảng `information`
--
ALTER TABLE `information`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `news__evens`
--
ALTER TABLE `news__evens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `noiquy`
--
ALTER TABLE `noiquy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `style_advs`
--
ALTER TABLE `style_advs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `style_images`
--
ALTER TABLE `style_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `style_rooms`
--
ALTER TABLE `style_rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `tours`
--
ALTER TABLE `tours`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `advertisements`
--
ALTER TABLE `advertisements`
  ADD CONSTRAINT `StyleAdvserment` FOREIGN KEY (`styleAdv_id`) REFERENCES `style_advs` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Các ràng buộc cho bảng `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_id_loaiphong_foreign` FOREIGN KEY (`id_loaiphong`) REFERENCES `style_rooms` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_id_loaiphong_foreign` FOREIGN KEY (`id_loaiPhong`) REFERENCES `style_rooms` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
